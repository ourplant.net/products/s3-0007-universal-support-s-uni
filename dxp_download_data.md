Here, you will find an overview of the open source information of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s3-0007-universal-support-s-uni).

| document                 | download options |
| :----------------------- | ---------------: |
| operating manual         |[de](https://gitlab.com/ourplant.net/products/s3-0007-universal-support-s-uni/-/raw/main/01_operating_manual/S3-0007_A1_Betriebsanleitung.pdf), [en](https://gitlab.com/ourplant.net/products/s3-0007-universal-support-s-uni/-/raw/main/01_operating_manual/S3-0007_A_Operating%20Manual.pdf)                |
| assembly drawing         |[de](https://gitlab.com/ourplant.net/products/s3-0007-universal-support-s-uni/-/raw/main/02_assembly_drawing/s3-0007-a_universal_support_s-uni.PDF)                 |
| circuit diagram          |                  |
| maintenance instructions |                  |
| spare parts              |                  |

